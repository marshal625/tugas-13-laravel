@extends('layout.master')

@section('content')
	<div class="card card-primary">
	    <div class="card-header">
	        <h3 class="card-title">Add New Cast</h3>
	        </div>
	        <!-- /.card-header -->
	        <!-- form start -->
	         <form role="form" action="/cast" method="post">
	         	@csrf
	            <div class="card-body">
	                <div class="form-group">
	                	<label for="nama">Nama</label>
	                	<input type="text" class="form-control" id="nama" placeholder="Nama Cast" name='nama' value="{{old('nama', '')}}">
	                	@error('nama')
						    <div class="alert alert-danger">
						    	{{ $message }}
						    </div>
						@enderror
	                </div>
	                <div class="form-group">
	                    <label for="umur">Umur</label>
	                    <input type="number" class="form-control" id="umur" placeholder="Umur Cast" min="1" name="umur" value="{{old('umur', '')}}">
	                    @error('umur')
						    <div class="alert alert-danger">
						    	{{ $message }}
						    </div>
						@enderror
	                </div>
	                <div class="form-group">
	                    <label for="bio">Bio</label>
	                    <input type="text" class="form-control" id="bio" placeholder="Bio" name="bio" value="{{old('bio', '')}}">
	                    @error('bio')
						    <div class="alert alert-danger">
						    	{{ $message }}
						    </div>
						@enderror
	                </div>
	                <!-- /.card-body -->

	                <div class="card-footer">
	                  <button type="submit" class="btn btn-primary">Add</button>
	                </div>
	            </form>
	        </div>
@endsection