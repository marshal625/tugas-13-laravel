@extends('layout.master')

@section('content')
	<div class="card">
              <div class="card-header">
                <h3 class="card-title">Casts List</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              	@if(session('success'))
              		<div class="alert alert-success">
              			{{session('success')}}
              		</div>
              	@endif
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">ID</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th>Bio</th>
                      <th style="width: 40px">Tindakan</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($casts as $key => $casts)
                    	<tr>
                    		<td>{{$casts->id}}</td>
                    		<td>{{$casts->nama}}</td>
                    		<td>{{$casts->umur}}</td>
                    		<td>{{$casts->bio}}</td>
                    		<td style="display: flex;">
                    			<a class="btn btn-info btn-sm m-1" href="/cast/{{$casts->id}}">Detail</a>
                    			<a class="btn btn-warning btn-sm m-1" href="/cast/{{$casts->id}}/edit">Edit</a>
                    			<form action="/cast/{{$casts->id}}" method="post">
                    				@csrf
                    				@method('DELETE')
                    				<input type="submit" value="hapus" name="hapus" class="btn btn-sm btn-danger m-1">
                    			</form>
                    		</td>
                    	</tr>
                    @empty
                    	<tr>
                    		<td colspan="5" align="center">No Casts</td>
                    	</tr>
                    @endforelse
                  </tbody>
                </table>
                <a class="btn btn-primary m-3 float-right" href="/cast/create">Add Cast</a>
              </div>
              <!-- /.card-body -->
              <!--<div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>-->
            </div>
@endsection