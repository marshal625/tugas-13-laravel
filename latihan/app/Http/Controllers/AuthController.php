<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
     public function welcome(request $request){
        $namaAwal = strtoupper($request->firstname);
        $namaAkhir = strtoupper($request->lastname);
        return view('welcome' , compact('namaAwal' , 'namaAkhir'));
    }
}
