<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('casts.create');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')->insert([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"]
        ]);
        return redirect('/cast')->with('success', 'Cast Berhasil Ditambahkan!');
    }
    public function index(){
        $casts = DB::table('cast')->get();
        return view('casts.index', compact('casts'));
    }
     public function show($cast_id){
        $casts = DB::table('cast')->where('id', $cast_id)->first();  
        return view('casts.show', compact('casts'));
    }
    public function edit($cast_id){
        $casts = DB::table('cast')->where('id', $cast_id)->first();
        return view('casts.edit', compact('casts'));
    }
    public function update($cast_id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')
                ->where('id', $cast_id)
                ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast')->with('success', 'Data Berhasil Diperbarui!');
    }
    public function destroy($cast_id){
        $query = DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast')->with('success', 'Data Berhasil Dihapus!');
    }
}
